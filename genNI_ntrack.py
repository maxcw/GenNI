import numpy as np
from numpy import array
import pickle
from sklearn import linear_model,neural_network
from numpy import random
from helper_functions import means,means_and_stds
from explicitKernel import explicitKernel as EK

from optparse import OptionParser
parser = OptionParser()
parser.add_option("--rs1", help="random state of forward model",default=0,type=int)
parser.add_option("--fit1", help="redo fit of forward model",default=False,action="store_true")
parser.add_option("--rs2", help="random state of backward model",default=0,type=int)
parser.add_option("--fit2", help="redo fit of backward model",default=False,action="store_true")
parser.add_option("--plotDir", help="plot dir",default='/u/at/acukierm/u02_nfs/GenNI/MLGSC/figures_ntrack',type=str)
parser.add_option("--outDir", help="output dir",default='/u/at/acukierm/u02_nfs/GenNI/MLGSC/output_ntrack',type=str)

(opt, args) = parser.parse_args()

plotDir = opt.plotDir
outDir = opt.outDir

import pdb

epsilon=1e-5

random.seed(1)

data = np.load('data/data_small_0eta1.npy')
x = data[:,1]
y = data[:,0]
z = data[:,7] #ntrack

minx = 20
maxx = 60
y = y[np.all([x>minx,x<maxx],axis=0)]
z = z[np.all([x>minx,x<maxx],axis=0)]
x = x[np.all([x>minx,x<maxx],axis=0)]

midx = int(0.5*(minx+maxx))

maxnpv = max(z)
midnpv = np.median(z)

#givenz = np.zeros_like(x) #if want a hidden variable
givenz = z

Z = givenz.reshape(-1,1) #just reshape
X = x.reshape(-1,1) #just reshape
Y = y.reshape(-1,1) #just reshape

if False:

  import matplotlib.pyplot as plt
  from matplotlib import rc
  rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
  ## for Palatino and other serif fonts use:
  #rc('font',**{'family':'serif','serif':['Palatino']})
  rc('text', usetex=True)
  import matplotlib
  matplotlib.rcParams['font.size'] = 16
  matplotlib.rcParams['figure.dpi']= 300

  plt.scatter(x,y,color='b',label='Data',s=1)
  plt.xlabel('$p_T^{true}$ [GeV]')
  plt.ylabel('$p_T^{reco}$ [GeV]')
  plt.legend(loc='upper left')
  plt.savefig(plotDir+'/y_x.png')
  plt.close()

  plt.scatter(x,y/x,color='b',label='Data',s=1)
  plt.xlabel('$p_T^{true}$ [GeV]')
  plt.ylabel('$p_T^{reco}/p_T^{true}$')
  plt.legend(loc='upper left')
  plt.ylim(0,3)
  plt.savefig(plotDir+'/R_x.png')
  plt.close()

  plt.scatter(z,y/x,color='b',label='Data',s=1)
  plt.xlabel('NPV')
  plt.ylabel('$p_T^{reco}/p_T^{true}$')
  plt.legend(loc='upper left')
  plt.ylim(0,3)
  plt.savefig(plotDir+'/R_z.png')
  plt.close()

  meanxs,meanys = means(x,y/x,binsize=2)
  plt.plot(meanxs,meanys,color='black',ls='-',linewidth=3,label='All')
  meanxs,meanys = means(x,y/x,binsize=2,inds=(z<0+epsilon))
  plt.plot(meanxs,meanys,color='g',ls='-',linewidth=3,label='nTrack=0')
  meanxs,meanys = means(x,y/x,binsize=2,inds=np.all([z<3+epsilon,z>0+epsilon],axis=0))
  plt.plot(meanxs,meanys,color='r',ls='-',linewidth=3,label='0$<$nTrack$\le$3')
  meanxs,meanys = means(x,y/x,binsize=2,inds=z>3+epsilon)
  plt.plot(meanxs,meanys,color='b',ls='-',linewidth=3,label='nTrack$>$3')
  plt.xlabel('$p_T^{true}$ [GeV]')
  plt.ylabel('$p_T^{reco}/p_T^{true}$')
  plt.legend(loc='upper left')
  plt.ylim(0.8,1.4)
  plt.savefig(plotDir+'/R_x_scan.png')
  plt.close()

  meanxs,meanys = means(z,y/x,binsize=1)
  plt.plot(meanxs,meanys,color='black',ls='-',linewidth=3,label='All')
  meanxs,meanys = means(z,y/x,binsize=1,inds=np.all([x>20,x<30],axis=0))
  plt.plot(meanxs,meanys,color='r',ls='-',linewidth=3,label='20 GeV $< p_T^{true} <$ 30 GeV')
  meanxs,meanys = means(z,y/x,binsize=1,inds=np.all([x>30,x<40],axis=0))
  plt.plot(meanxs,meanys,color='g',ls='-',linewidth=3,label='30 GeV $< p_T^{true} <$ 40 GeV')
  meanxs,meanys = means(z,y/x,binsize=1,inds=np.all([x>40,x<50],axis=0))
  plt.plot(meanxs,meanys,color='orange',ls='-',linewidth=3,label='40 GeV $< p_T^{true} <$ 50 GeV')
  meanxs,meanys = means(z,y/x,binsize=1,inds=np.all([x>50,x<60],axis=0))
  plt.plot(meanxs,meanys,color='b',ls='-',linewidth=3,label='50 GeV $< p_T^{true} <$ 60 GeV')
  plt.xlabel('nTrack')
  plt.ylabel('$p_T^{reco}/p_T^{true}$')
  plt.ylim(0.8,1.4)
  plt.xlim(0,10)
  plt.legend(loc='upper right')
  plt.savefig(plotDir+'/R_z_scan.png')
  plt.close()

  meanxs,_,_,_,_,_,stdys,stdy_errs = means_and_stds(x,y/x,binsize=2,errs=True)
  plt.errorbar(meanxs,stdys,yerr=stdy_errs,c='g',fmt='o',markersize=6)
  plt.xlabel('$p_T^{true}$')
  plt.ylabel('$\sigma\left(p_T^{reco}\\right)/p_T^{true}$')
  plt.ylim(0,0.4)
  plt.savefig(plotDir+'/stdR_x_all.png')
  plt.close()

  meanxs,stdxs,meanys,stdys = means_and_stds(x,y/x,binsize=2)
  plt.plot(meanxs,stdys,color='black',ls='-',linewidth=3,label='All')
  meanxs,stdxs,meanys,stdys = means_and_stds(x,y/x,binsize=2,inds=(z<0+epsilon))
  plt.plot(meanxs,stdys,color='g',ls='-',linewidth=3,label='nTrack=0')
  meanxs,stdxs,meanys,stdys = means_and_stds(x,y/x,binsize=2,inds=np.all([z<3+epsilon,z>0+epsilon],axis=0))
  plt.plot(meanxs,stdys,color='r',ls='-',linewidth=3,label='0$<$nTrack$\le$3')
  meanxs,stdxs,meanys,stdys = means_and_stds(x,y/x,binsize=2,inds=z>3+epsilon)
  plt.plot(meanxs,stdys,color='b',ls='-',linewidth=3,label='nTrack$>$3')
  plt.xlabel('$p_T^{true}$ [GeV]')
  plt.ylabel('$\sigma\left(p_T^{reco}\\right)/p_T^{true}$')
  plt.legend(loc='upper right')
  plt.ylim(0,0.4)
  plt.savefig(plotDir+'/stdR_x_scan.png')
  plt.close()

  meanxs,_,_,_,_,_,stdys,stdy_errs = means_and_stds(z,y/x,binsize=1,errs=True)
  plt.errorbar(meanxs,stdys,yerr=stdy_errs,c='g',fmt='o',markersize=6)
  plt.xlabel('nTrack')
  plt.ylabel('$\sigma\left(p_T^{reco}\\right)/p_T^{true}$')
  plt.ylim(0,0.6)
  plt.xlim(0,10)
  plt.savefig(plotDir+'/stdR_z_all.png')
  plt.close()

  meanxs,stdxs,meanys,stdys = means_and_stds(z,y/x,binsize=1)
  plt.plot(meanxs,stdys,color='black',ls='-',linewidth=3,label='All')
  meanxs,stdxs,meanys,stdys = means_and_stds(z,y/x,binsize=1,inds=np.all([x>20,x<30],axis=0))
  plt.plot(meanxs,stdys,color='r',ls='-',linewidth=3,label='20 GeV $< p_T^{true} <$ 30 GeV')
  meanxs,stdxs,meanys,stdys = means_and_stds(z,y/x,binsize=1,inds=np.all([x>30,x<40],axis=0))
  plt.plot(meanxs,stdys,color='g',ls='-',linewidth=3,label='30 GeV $< p_T^{true} <$ 40 GeV')
  meanxs,stdxs,meanys,stdys = means_and_stds(z,y/x,binsize=1,inds=np.all([x>40,x<50],axis=0))
  plt.plot(meanxs,stdys,color='orange',ls='-',linewidth=3,label='40 GeV $< p_T^{true} <$ 50 GeV')
  meanxs,stdxs,meanys,stdys = means_and_stds(z,y/x,binsize=1,inds=np.all([x>50,x<60],axis=0))
  plt.plot(meanxs,stdys,color='b',ls='-',linewidth=3,label='50 GeV $< p_T^{true} <$ 60 GeV')
  plt.xlabel('nTrack')
  plt.ylabel('$\sigma\left(p_T^{reco}\\right)/p_T^{true}$')
  plt.legend(loc='upper right')
  plt.ylim(0,0.6)
  plt.xlim(0,10)
  plt.savefig(plotDir+'/stdR_z_scan.png')
  plt.close()

dataExists = not opt.fit1 and not opt.fit2

outName = outDir+'/Cdata'+'_rs1_'+str(opt.rs1)+'_rs2_'+str(opt.rs2)+'.npy'
if dataExists:
  try:
    data = np.load(outName)
    Lx = data['Lx']
    Tx = data['Tx']
    Cy = data['Cy']
    print '>> Using saved data'
  except:
    print '>> Couldn\'t find saved data - fitting'
    dataExists = False
if not dataExists:
  #Learn reco pT given true pT and features:
  #sometimes have to cycle through a few random states to get a good fit
  outName1 = outDir+'/lr'+'_rs1_'+str(opt.rs1)+'.p'
  if not opt.fit1:
    try:
      lr = np.load(outName1)
      print '>> Using prefit forward model'
    except:
      opt.fit1 = True
      print '>> Can\'t find prefit forward model'
  if opt.fit1:
    print '>> Fitting forward model'
    lr = EK(neural_network.MLPRegressor(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(100,100,), random_state=opt.rs1,warm_start=False,activation='relu',max_iter=2000),2) #explicit kernel around MLPRegressor
    #lr = EK(neural_network.MLPRegressor(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(100,), random_state=opt.rs1,warm_start=False,activation='relu',max_iter=100),2) #explicit kernel around MLPRegressor
    lr.fit(np.hstack([X,Z]),y)
    pickle.dump(lr,open(outName1,'wb'))
  print 'Learning loss:',lr.model.loss_

  xx = 20
  #testX = np.arange(xx,xx+5,1.).reshape(-1,1)
  #testZ = np.zeros_like(testX)
  #testpred = lr.predict(np.hstack([testX,testZ]))
  #dtestpred = testpred[1:]-testpred[:-1]
  #print 'Test predictions',testpred
  #_,meanys = means(x,y,inds=np.all([x>xx,x<xx+5,givenz==0],axis=0),binsize=1.)
  #print meanys
  #print testpred/meanys
  pred = lr.predict(np.hstack([X,Z]))
  _,meanRs = means(x,y/pred,inds=np.all([x>xx,x<xx+5],axis=0),binsize=1.)
  print meanRs
  #quit()
  #print f(testX,midnpv).reshape(-1)/testpred
  #inds = np.all([x<xx+1,x>xx],axis=0)
  #print np.mean(y[inds]),np.std(y[inds])/np.sqrt(sum(inds))
  #print 'Test predictions derivative',dtestpred

  #Do inversion:
  from genNumericalInversion import genNumericalInverter
  #help(genNumericalInverter)

  #sometimes have to cycle through a few random states to get a good fit
  outName2 = outDir+'/gNI'+'_rs1_'+str(opt.rs1)+'_rs2_'+str(opt.rs2)+'.p'
  if not opt.fit2:
    try:
      gNI = np.load(outName2)
      print '>> Using prefit backward model'
    except:
      opt.fit2 = True
      print '>> Can\'t find prefit backward model'
  if opt.fit2:
    print '>> Fitting backward model'
    lr2 = EK(neural_network.MLPRegressor(solver='lbfgs', alpha=0,hidden_layer_sizes=(100,100,), random_state=opt.rs2,warm_start=False,activation='relu',max_iter=2000,learning_rate='constant'),2)
    gNI = genNumericalInverter(lr,lr2) #initialize gNI
    gNI.fit(X,Z) #do inversion fit
    pickle.dump(gNI,open(outName2,'wb'))

  Lx = lr.predict(np.hstack([X,Z])) #test technical closure on learned reco values
  #Tx = gNI.technical_closure(returnArr=True)
  Tx = gNI.predict(Lx,Z)

  #Z_reco = np.hstack([givenz.reshape(-1,1),(y*givenz).reshape(-1,1)]) #explicit kernel to capture correlations
  Cy = gNI.predict(Y,Z) #physical closure using new calibration function on reco values

  np.savez(open(outName,'wb'),x=x,y=y,z=z,Lx=Lx,Tx=Tx,Cy=Cy)

print 'Technical closure: ',np.mean(Tx/x),np.std(Tx/x) #confirms inversion has worked
print 'Closure: ',np.mean(Cy/x),np.std(Cy/x) #any residual nonclosures can result from original model not learning reco pT or inherent nonclosures in numerical inversion

meanxs,_,_,_,_,_,stdys,stdy_errs = means_and_stds(x,Cy/x,binsize=2,errs=True)
print 'Resolution: '
print stdys

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
import matplotlib
matplotlib.rcParams['font.size'] = 16
matplotlib.rcParams['figure.dpi']= 300

if True:
  for zz in [-1,0,1,2,3,4,5,6,7,8]:
    inds=np.all([z>zz+epsilon,z<zz+1+epsilon],axis=0)
    meanxs,_,_,_,meanys,meany_errs,_,_ = means_and_stds(x,y/x,binsize=2,inds=inds,errs=True)
    plt.scatter(x[inds],(y/x)[inds],s=1,color='b',label='Data')
    plt.scatter(x[inds],(Lx/x)[inds],s=1,color='g',label='Learned')
    plt.errorbar(meanxs,meanys,yerr=meany_errs,fmt='o',color='r',markersize=4,label='Binned Means')
    plt.text(20,1.8,'nTrack='+str(zz+1))
    plt.xlabel('$p_T^{true}$ [GeV]')
    plt.ylabel('$p_T^{reco}/p_T^{true}$')
    plt.legend(loc='upper right')
    plt.ylim(0,2)
    plt.savefig(plotDir+'/LR_x_'+'z'+str(zz+1)+'.png')
    plt.close()

  for xx in [20,25,30,40,50]:
    inds = np.all([x>xx,x<xx+5],axis=0)
    meanzs,_,_,_,meanys,meany_errs,_,_ = means_and_stds(z,y/x,binsize=1,inds=inds,errs=True)
    plt.scatter(z[inds],(y/x)[inds],s=1,color='b',label='Data')
    plt.scatter(z[inds],(Lx/x)[inds],s=1,color='g',label='Learned')
    plt.errorbar(meanzs,meanys,yerr=meany_errs,fmt='o',color='r',markersize=4,label='Binned Means')
    plt.text(0,1.8,str(xx)+' GeV $< p_T^{true} <$ '+str(xx+5)+' GeV')
    plt.xlabel('nTrack')
    plt.ylabel('$p_T^{reco}/p_T^{true}$')
    plt.legend(loc='upper right')
    plt.ylim(0,2)
    plt.xlim(0,10)
    plt.savefig(plotDir+'/LR_z_'+str(xx)+'x'+str(xx+5)+'.png')
    plt.close()

  meanxs,meanys = means(x,Lx/x,binsize=2)
  plt.plot(meanxs,meanys,color='black',ls='-',linewidth=3,label='All')
  meanxs,meanys = means(x,Lx/x,binsize=2,inds=(z<0+epsilon))
  plt.plot(meanxs,meanys,color='g',ls='-',linewidth=3,label='nTrack=0')
  meanxs,meanys = means(x,Lx/x,binsize=2,inds=np.all([z<3+epsilon,z>0+epsilon],axis=0))
  plt.plot(meanxs,meanys,color='r',ls='-',linewidth=3,label='0$<$nTrack$\le$3')
  meanxs,meanys = means(x,Lx/x,binsize=2,inds=z>3+epsilon)
  plt.plot(meanxs,meanys,color='b',ls='-',linewidth=3,label='nTrack$>$3')
  plt.xlabel('$p_T^{true}$ [GeV]')
  plt.ylabel('$p_T^{reco}/p_T^{true}$')
  plt.legend(loc='upper left')
  plt.ylim(0.8,1.4)
  plt.savefig(plotDir+'/LR_x_scan.png')
  plt.close()

  meanxs,meanys = means(z,Lx/x,binsize=1)
  plt.plot(meanxs,meanys,color='black',ls='-',linewidth=3,label='All')
  meanxs,meanys = means(z,Lx/x,binsize=1,inds=np.all([x>20,x<30],axis=0))
  plt.plot(meanxs,meanys,color='r',ls='-',linewidth=3,label='20 GeV $< p_T^{true} <$ 30 GeV')
  meanxs,meanys = means(z,Lx/x,binsize=1,inds=np.all([x>30,x<40],axis=0))
  plt.plot(meanxs,meanys,color='g',ls='-',linewidth=3,label='30 GeV $< p_T^{true} <$ 40 GeV')
  meanxs,meanys = means(z,Lx/x,binsize=1,inds=np.all([x>40,x<50],axis=0))
  plt.plot(meanxs,meanys,color='orange',ls='-',linewidth=3,label='40 GeV $< p_T^{true} <$ 50 GeV')
  meanxs,meanys = means(z,Lx/x,binsize=1,inds=np.all([x>50,x<60],axis=0))
  plt.plot(meanxs,meanys,color='b',ls='-',linewidth=3,label='50 GeV $< p_T^{true} <$ 60 GeV')
  plt.xlabel('nTrack')
  plt.ylabel('$p_T^{reco}/p_T^{true}$')
  plt.legend(loc='upper left')
  plt.ylim(0.8,1.4)
  plt.xlim(0,10)
  plt.savefig(plotDir+'/LR_z_scan.png')
  plt.close()

if True:
  testx = np.arange(20,60,0.1)
  for zz in [-1,0,1,2,3,4,5,6,7,8]:
    inds=np.all([z>zz+epsilon,z<zz+1+epsilon],axis=0)
    meanxs,_,_,_,meanys,meany_errs,_,_ = means_and_stds(x,Tx/x,binsize=2,inds=inds,errs=True)
    plt.plot(testx,np.ones_like(testx),ls='--',color='black')
    plt.errorbar(meanxs,meanys,yerr=meany_errs,fmt='o',color='r',markersize=4)
    plt.text(20,1.8,'nTrack='+str(zz+1))
    plt.xlabel('$p_T^{true}$ [GeV]')
    plt.ylabel('$p_T^{reco}/p_T^{true}$')
    plt.ylim(0.9,1.1)
    plt.savefig(plotDir+'/TR_x_'+'z'+str(zz+1)+'.png')
    plt.close()

  testz = np.arange(0,maxnpv+1,1)
  for xx in [20,25,30,40,50]:
    inds = np.all([x>xx,x<xx+5],axis=0)
    meanzs,_,_,_,meanys,meany_errs,_,_ = means_and_stds(z,Tx/x,binsize=1,inds=inds,errs=True)
    plt.plot(testz,np.ones_like(testz),ls='--',color='black')
    plt.errorbar(meanzs,meanys,yerr=meany_errs,fmt='o',color='r',markersize=4)
    plt.text(0,1.8,str(xx)+' GeV $< p_T^{true} <$ '+str(xx+5)+' GeV')
    plt.xlabel('nTrack')
    plt.ylabel('$p_T^{reco}/p_T^{true}$')
    plt.ylim(0,2)
    plt.xlim(0,10)
    plt.savefig(plotDir+'/TR_z_'+str(xx)+'x'+str(xx+5)+'.png')
    plt.close()

if True:
  plt.scatter(x,Cy/x,color='b',label='Data',s=1)
  plt.xlabel('$p_T^{true}$ [GeV]')
  plt.ylabel('$C(p_T^{reco})/p_T^{true}$')
  plt.legend(loc='upper left')
  plt.ylim(0,3)
  plt.savefig(plotDir+'/CR_x_all.png')
  plt.close()

  plt.scatter(z,Cy/x,color='b',label='Data',s=1)
  plt.xlabel('nTrack')
  plt.ylabel('$C(p_T^{reco})/p_T^{true}$')
  plt.legend(loc='upper left')
  plt.ylim(0,3)
  plt.savefig(plotDir+'/CR_z_all.png')
  plt.close()

  meanxs,meanys = means(x,Cy/x,binsize=2)
  plt.plot(meanxs,meanys,color='black',ls='-',linewidth=3,label='All')
  meanxs,meanys = means(x,Cy/x,binsize=2,inds=(z<0+epsilon))
  plt.plot(meanxs,meanys,color='g',ls='-',linewidth=3,label='nTrack=0')
  meanxs,meanys = means(x,Cy/x,binsize=2,inds=np.all([z<3+epsilon,z>0+epsilon],axis=0))
  plt.plot(meanxs,meanys,color='r',ls='-',linewidth=3,label='0$<$nTrack$\le$3')
  meanxs,meanys = means(x,Cy/x,binsize=2,inds=z>3+epsilon)
  plt.plot(meanxs,meanys,color='b',ls='-',linewidth=3,label='nTrack$>$3')
  plt.xlabel('$p_T^{true}$ [GeV]')
  plt.ylabel('$C(p_T^{reco})/p_T^{true}$')
  plt.legend(loc='upper left')
  plt.ylim(0.8,1.4)
  plt.savefig(plotDir+'/CR_x_scan.png')
  plt.close()

  meanxs,meanys = means(z,Cy/x,binsize=1)
  plt.plot(meanxs,meanys,color='black',ls='-',linewidth=3,label='All')
  meanxs,meanys = means(z,Cy/x,binsize=1,inds=np.all([x>20,x<30],axis=0))
  plt.plot(meanxs,meanys,color='r',ls='-',linewidth=3,label='20 GeV $< p_T^{true} <$ 30 GeV')
  meanxs,meanys = means(z,Cy/x,binsize=1,inds=np.all([x>30,x<40],axis=0))
  plt.plot(meanxs,meanys,color='g',ls='-',linewidth=3,label='30 GeV $< p_T^{true} <$ 40 GeV')
  meanxs,meanys = means(z,Cy/x,binsize=1,inds=np.all([x>40,x<50],axis=0))
  plt.plot(meanxs,meanys,color='orange',ls='-',linewidth=3,label='40 GeV $< p_T^{true} <$ 50 GeV')
  meanxs,meanys = means(z,Cy/x,binsize=1,inds=np.all([x>50,x<60],axis=0))
  plt.plot(meanxs,meanys,color='b',ls='-',linewidth=3,label='50 GeV $< p_T^{true} <$ 60 GeV')
  plt.xlabel('nTrack')
  plt.ylabel('$C(p_T^{reco})/p_T^{true}$')
  plt.ylim(0.8,1.4)
  plt.xlim(0,10)
  plt.legend(loc='upper right')
  plt.savefig(plotDir+'/CR_z_scan.png')
  plt.close()

  for zz in [-1,0,1,2,3,4,5,6,7,8]:
    inds=np.all([z>zz+epsilon,z<zz+1+epsilon],axis=0)
    meanxs,_,_,_,meanys,meany_errs,_,_ = means_and_stds(x,Cy/x,binsize=2,inds=inds,errs=True)
    plt.errorbar(meanxs,meanys,yerr=meany_errs,fmt='o',color='r',markersize=4)
    plt.text(20,1.8,'nTrack='+str(zz+1))
    plt.xlabel('$p_T^{true}$ [GeV]')
    plt.ylabel('$C(p_T^{reco})/p_T^{true}$')
    plt.ylim(0.8,1.4)
    plt.savefig(plotDir+'/CR_x_'+'z'+str(zz+1)+'.png')
    plt.close()

  for xx in [20,25,30,40,50]:
    inds = np.all([x>xx,x<xx+5],axis=0)
    meanzs,_,_,_,meanys,meany_errs,_,_ = means_and_stds(z,Cy/x,binsize=1,inds=inds,errs=True)
    plt.errorbar(meanzs,meanys,yerr=meany_errs,fmt='o',color='r',markersize=4)
    plt.text(0,1.8,str(xx)+' GeV $< p_T^{true} <$ '+str(xx+5)+' GeV')
    plt.xlabel('nTrack')
    plt.ylabel('$C(p_T^{reco})/p_T^{true}$')
    plt.ylim(0.8,1.4)
    plt.xlim(0,10)
    plt.savefig(plotDir+'/CR_z_'+str(xx)+'x'+str(xx+5)+'.png')
    plt.close()

if True:
  meanxs,_,_,_,_,_,stdys,stdy_errs = means_and_stds(x,Cy/x,binsize=2,errs=True)
  plt.errorbar(meanxs,stdys,yerr=stdy_errs,c='g',fmt='o',markersize=6)
  plt.xlabel('$p_T^{true}$')
  plt.ylabel('$\sigma\left(C(p_T^{reco})\\right)/p_T^{true}$')
  plt.ylim(0,0.4)
  plt.savefig(plotDir+'/stdCR_x_all.png')
  plt.close()

  meanxs,stdxs,meanys,stdys = means_and_stds(x,Cy/x,binsize=2)
  plt.plot(meanxs,stdys,color='black',ls='-',linewidth=3,label='All')
  meanxs,stdxs,meanys,stdys = means_and_stds(x,Cy/x,binsize=2,inds=(z<0+epsilon))
  plt.plot(meanxs,stdys,color='g',ls='-',linewidth=3,label='nTrack=0')
  meanxs,stdxs,meanys,stdys = means_and_stds(x,Cy/x,binsize=2,inds=np.all([z<3+epsilon,z>0+epsilon],axis=0))
  plt.plot(meanxs,stdys,color='r',ls='-',linewidth=3,label='0$<$nTrack$\le$3')
  meanxs,stdxs,meanys,stdys = means_and_stds(x,Cy/x,binsize=2,inds=z>3+epsilon)
  plt.plot(meanxs,stdys,color='b',ls='-',linewidth=3,label='nTrack$>$3')
  plt.xlabel('$p_T^{true}$ [GeV]')
  plt.ylabel('$\sigma\left(C(p_T^{reco})\\right)/p_T^{true}$')
  plt.legend(loc='upper right')
  plt.ylim(0,0.4)
  plt.savefig(plotDir+'/stdCR_x_scan.png')
  plt.close()

  meanxs,_,_,_,_,_,stdys,stdy_errs = means_and_stds(z,Cy/x,binsize=1,errs=True)
  plt.errorbar(meanxs,stdys,yerr=stdy_errs,c='g',fmt='o',markersize=6)
  plt.xlabel('nTrack')
  plt.ylabel('$\sigma\left(C(p_T^{reco})\\right)/p_T^{true}$')
  plt.ylim(0,0.6)
  plt.xlim(0,10)
  plt.savefig(plotDir+'/stdCR_z_all.png')
  plt.close()

  meanxs,stdxs,meanys,stdys = means_and_stds(z,Cy/x,binsize=1)
  plt.plot(meanxs,stdys,color='black',ls='-',linewidth=3,label='All')
  meanxs,stdxs,meanys,stdys = means_and_stds(z,Cy/x,binsize=1,inds=np.all([x>20,x<30],axis=0))
  plt.plot(meanxs,stdys,color='r',ls='-',linewidth=3,label='20 GeV $< p_T^{true} <$ 30 GeV')
  meanxs,stdxs,meanys,stdys = means_and_stds(z,Cy/x,binsize=1,inds=np.all([x>30,x<40],axis=0))
  plt.plot(meanxs,stdys,color='g',ls='-',linewidth=3,label='30 GeV $< p_T^{true} <$ 40 GeV')
  meanxs,stdxs,meanys,stdys = means_and_stds(z,Cy/x,binsize=1,inds=np.all([x>40,x<50],axis=0))
  plt.plot(meanxs,stdys,color='orange',ls='-',linewidth=3,label='40 GeV $< p_T^{true} <$ 50 GeV')
  meanxs,stdxs,meanys,stdys = means_and_stds(z,Cy/x,binsize=1,inds=np.all([x>50,x<60],axis=0))
  plt.plot(meanxs,stdys,color='b',ls='-',linewidth=3,label='50 GeV $< p_T^{true} <$ 60 GeV')
  plt.xlabel('nTrack')
  plt.ylabel('$\sigma\left(C(p_T^{reco})\\right)/p_T^{true}$')
  plt.legend(loc='upper right')
  plt.ylim(0,0.6)
  plt.xlim(0,10)
  plt.savefig(plotDir+'/stdCR_z_scan.png')
  plt.close()
