import numpy as np

class genNumericalInverter():
  '''
  A tool for performing generalized numerical inversion for jet calibration with arbitrary number of features. 
  See https://indico.cern.ch/event/691745/contributions/2893042/attachments/1599321/2536326/GenNI_2.13.18.pdf for details.
  '''
  #======
  def __init__(self,learned_model,inversion_model):
    '''
    genNumericalInverter(learned_model,inversion_model)
    learned_model - previously trained model learning reco pTs given true pTs and other_features
    inversion_model - untrained model architecture for learning inversion (not necessarily same architecture as learned_model)
    '''
    self.lm = learned_model
    if not hasattr(self.lm,'predict'): raise AttributeError('*** AttributeError: learned_model must be a model with a predict() function')

    self.model = inversion_model
    if not hasattr(self.model,'fit'): raise AttributeError('*** AttributeError: inversion_model must be a model with a fit() function')
    if not hasattr(self.model,'predict'): raise AttributeError('*** AttributeError: inversion_model must be a model with a predict() function')

    self.fitted = False
    return
  #========

  #========
  def fit(self,true_values,other_features): 
    '''
    genNumericalInverter.fit(true_values,other_features)
    true_values - array of true pT values (can be row or column vector)
    other_features - matrix of other features (should be horizontally stacked array of column vectors)
    Does the inversion.
    Returns fitted inversion model, which is also accessible as genNumericalInverter.model.
    '''
    if len(true_values.shape)==1: pass #true_values is a row vector
    elif len(true_values.shape)==2 and true_values.shape[1]==1: #true_values is a column vector
      true_values = true_values.reshape(-1) #true_values is a row vector
    else: raise AssertionError('true_values has to be a row or column vector of values to learn from')

    self.maxx = max(true_values) #needed for linear extrapolation
    self.minx = min(true_values) #needed for linear extrapolation
    self.p99x = np.percentile(true_values,99.) #needed for linear extrapolation
    self.p01x = np.percentile(true_values,1.) #needed for linear extrapolation

    X = true_values.reshape(-1,1) #self.X is a column vector
    Z = other_features #not including reco pT
    if not len(Z.shape)==len(X.shape): raise AssertionError('other_features has to be horizontally stacked column vectors of other features')
    if not len(X)==len(Z): raise AssertionError('true_values and other_features must be same length!')
    try: Y = self.lm.predict(np.hstack([X,Z]))
    except: raise RuntimeError('Tried to predict using learned_model on true_values and other_features and failed')
    Y = Y.reshape(-1,1) #column vector

    print 'Inverting...'
    self.model.fit(np.hstack([Y,Z]),X.reshape(-1)) #will just crash if this doesn't work
    if hasattr(self.model,'loss_'): print 'Done fitting inversion! Loss: ',self.model.loss_

    self.fitted = True

    pred = self.predict(Y,Z)
    closure = pred/X.reshape(-1)
    mean = np.mean(closure)
    std = np.std(closure)
    print 'Technical closure:',mean,std

    return self.model
  #========

  #======= 
  def predict(self,values,other_features):
    '''
    genNumericalInverter.predict(values,other_features)
    After inversion, makes predictions with new calibration function. Uses a linear extrapolation for pT values outside its training set.
    values - jet pTs to calibrate (can be row or column vector)
    other_features - horizontally stacked column vectors of other features
    Returns predicted calibrated values.
    '''
    if not self.fitted: raise RuntimeError('Have to fit first before predicting!')

    lm = self.lm
    lm2 = self.model

    try: assert(len(values)==len(other_features))
    except AssertionError: print '*** AssertionError: values and other_features must be same length!'
    if len(values.shape)==1: pass #values is a row vector
    elif len(values.shape)==2 and values.shape[1]==1: #values is a column vector
      values = values.reshape(-1) #values is a row vector
    else: raise AssertionError('values has to be a row or column vector of values to invert')
    Y = values.reshape(-1,1) #Y is a column vector
    Z = other_features
    if not (len(Z.shape)==len(Y.shape) and len(Z)==len(Y)): raise AssertionError('other_features has to be horizontally stacked column vectors of other features')

    #miny and maxy are predictive limits of learned_model
    # note: if test set has features Z that were never seen in training, then not good behavior
    maxY = lm.predict(np.hstack([self.maxx*np.ones_like(Y),Z])).reshape(-1,1) #will just crash if this doesn't work
    p99Y = lm.predict(np.hstack([self.p99x*np.ones_like(Y),Z])).reshape(-1,1) #will just crash if this doesn't work
    minY = lm.predict(np.hstack([self.minx*np.ones_like(Y),Z])).reshape(-1,1) #will just crash if this doesn't work
    p01Y = lm.predict(np.hstack([self.p01x*np.ones_like(Y),Z])).reshape(-1,1) #will just crash if this doesn't work
    #print maxY,minY
    
    #derivatives given Z at limits of learned_model
    H_max = lm2.predict(np.hstack([maxY,Z]))
    H_p99 = lm2.predict(np.hstack([p99Y,Z]))
    dHdy_up = (H_max-H_p99)/(maxY-p99Y).reshape(-1)
    H_min = lm2.predict(np.hstack([minY,Z]))
    H_p01 = lm2.predict(np.hstack([p01Y,Z]))
    dHdy_down = (H_p01-H_min)/(p01Y-minY).reshape(-1)
    # to-do: just return inds with positive derivative
    
    minY = minY.reshape(-1) #row vector
    maxY = maxY.reshape(-1) #row vector

    #linear extrapolation outside of predictive limits
    case1 = np.where(values>maxY)
    case2 = np.where(values<minY)
    case3 = np.where(np.logical_not(np.any([values>maxY.reshape(-1),values<minY.reshape(-1)],axis=0)))
    
    result = np.zeros_like(values) #row vector
    if len(case1[0])>0: result[case1] = H_max[case1]+dHdy_up[case1]*(values[case1]-maxY[case1])
    if len(case2[0])>0: result[case2] = H_min[case2]+dHdy_down[case2]*(values[case2]-minY[case2])
    if len(case3[0])>0: result[case3] = lm2.predict(np.hstack([Y[case3],Z[case3]]))

    return result #row vector
  #=======
