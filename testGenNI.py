import numpy as np
from sklearn import linear_model,neural_network
from numpy import random
#from explicitKernel import explicitKernel as EK
import pdb

def f1(x):
    return x*(np.log(x))/8. #simple nonlinear function
    
def f(x,h):
    return 0.01+h+f1(x) #simple dependence on other feature

random.seed(1)
N = 10000
minx = 20
maxx = 100
x = random.uniform(minx,maxx,N) #true pTs
maxnpv = 50
midnpv = int(0.5*maxnpv)
z = random.uniform(0,maxnpv,N) #other correlated feature
#givenz = np.zeros(N) #if want a hidden variable
givenz = z
y = f(x,z)+random.normal(0,7,N) #reco pTs

#Z = np.hstack([givenz.reshape(-1,1),(x*givenz).reshape(-1,1)]) #explicit kernel to capture correlations
X = x.reshape(-1,1) #just reshape
Z = givenz.reshape(-1,1) #just reshape
Y = y.reshape(-1,1) #just reshape

#Learn reco pT given true pT and features:
#sometimes have to cycle through a few random states to get a good fit
bestrs = 0
bestloss = float('inf')
bestlr = 0
#rss = range(0,10)
rss = [6]
lrs = {rs:neural_network.MLPRegressor(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(100,), random_state=rs,warm_start=False,activation='relu',max_iter=2000) for rs in rss}

for rs in rss:
    lrs[rs].fit(np.hstack([X,Z]),y)
    loss = lrs[rs].loss_
    #print rs,loss
    if loss<bestloss:
        bestloss = loss
        bestrs = rs
print 'Learning loss:'
print bestloss
lr = lrs[bestrs]
#print sum(pow(y-f(x,z),2))/(2*N)

#Do inversion:
from genNumericalInversion import genNumericalInverter
help(genNumericalInverter)

#sometimes have to cycle through a few random states to get a good fit
#rss = range(0,10)
rs = 0
rss = [rs]
lr2s = {rs:neural_network.MLPRegressor(solver='lbfgs', alpha=0,hidden_layer_sizes=(100,100,), random_state=rs,warm_start=False,activation='relu',max_iter=2000,learning_rate='constant') for rs in rss} #note that inversion model is not necessarily the same as original model
lr2 = lr2s[rs]

gNI = genNumericalInverter(lr,lr2) #initialize gNI
gNI.fit(X,Z) #do inversion fit
#print gNI.technical_closure() #confirms inversion has worked

Lx = lr.predict(np.hstack([X,Z])) #test technical closure on learned reco values
Tx = gNI.predict(Lx,Z)/x
print 'Testing technical closure: ',np.mean(Tx),np.std(Tx)

#Z_reco = np.hstack([givenz.reshape(-1,1)]) #explicit kernel to capture correlations
closure = gNI.predict(Y,Z)/x #physical closure using new calibration function on reco values
print 'Closure: ',np.mean(closure),np.std(closure) #any residual nonclosures can result from original model not learning reco pT or inherent nonclosures in numerical inversion

#Can save gNI object:
import pickle
pickle.dump(gNI,open('gNI.p','wb'))
gNI2 = np.load('gNI.p')
closure2 = gNI.predict(Y,Z)/x #physical closure using new calibration function on reco values
if np.array_equal(closure,closure2): print 'Can save gNI object and load it'

#Testing arbitrary examples:
testY = np.arange(f(minx,0)-5,f(maxx,0)+5,1).reshape(-1,1)
testZ = np.zeros_like(testY)
testpred = gNI.predict(testY,testZ)
dtestpred = testpred[1:]-testpred[:-1]
print
print 'Test values',testY.reshape(-1)
print 'Test predictions',testpred
#Learns nonlinear response:
print 'Test predictions derivative',dtestpred

#Testing linearity to -infinity:
testY = np.arange(f(minx,0)-(maxx+minx+5),f(minx,0)-(maxx+minx),1).reshape(-1,1)
testZ = np.zeros_like(testY)
testpred = gNI.predict(testY,testZ)
dtestpred = testpred[1:]-testpred[:-1]
print
print 'Test values',testY.reshape(-1)
print 'Test predictions',testpred
print 'Test predictions derivative',dtestpred

#Testing linearity to +infinity:
testY = np.arange(f(maxx,0)+(maxx+minx),f(maxx,0)+(maxx+minx+5),1).reshape(-1,1)
testZ = np.zeros_like(testY)
testpred = gNI.predict(testY,testZ)
dtestpred = testpred[1:]-testpred[:-1]
print
print 'Test values',testY.reshape(-1)
print 'Test predictions',testpred
print 'Test predictions derivative',dtestpred
